import math
def integers_from_start_to_end_using_range(start, end, step):
    p=[]
    for i in range(start,end,step):
        p.append(i)
    return p    

    """return a list"""
    #pass


def integers_from_start_to_end_using_while(start, end, step):
    a=(abs((abs(end)-abs(start))/step))
    p=[]
    if a!=int(a):
        a+=1
    b=1
    while b<=a:
        p.append(start)
        start=start+step
        b+=1
    return p    
    """return a list"""
    #pass

def prime_checker(i):
    for k in range(2,i):
        if i%k==0:
            return False
    return True
def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    m = []
    for i in range(10,100):
        if prime_checker(i) == True:
            m = m + [i]
    return m
    pass
