def last_3_characters(x):
    return x[-3:]
    #pass

def first_10_characters(x):
    return x[:10]
    #pass

def chars_4_through_10(x):
    return x[4:11]    
    #pass

def str_length(x):
    return len(x)   
    #pass


def words(x):
    y=[x]
    return y[0].split()
    #pass


def capitalize(x):
    return x.capitalize()
    #pass


def to_uppercase(x):
    return x.upper()
    #pass
