def is_prime(num):
    if num > 1:
        for k in range(2, num):
            if (num % k) == 0:
                return False
        else:
            return True
    else:
        return False


def n_digit_primes(digit):
    m = []
    for i in range(int('1' + '0'*(digit-1)), int('1' + '0'*(digit))):
        if is_prime(i):
            m = m + [i]
    return m
