def word_count(s):
    dictionary={}
    clean_string = s.replace('.',' ').replace(', ',' ').replace('. ',' ').split()
    words=list(set(clean_string))
    for i in words:
        dictionary[i]=clean_string.count(i)
    return dictionary    


    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    pass


def dict_items(d):
    p = [(t,u) for t,u in d.items()]
    return p
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    pass


def dict_items_sorted(d):
    p = [(t,u) for t,u in d.items()]
    p.sort()
    return p
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    pass
