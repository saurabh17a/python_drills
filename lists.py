def sum_items_in_list(x):
    return sum(x)
    #pass


def list_length(x):
    return len(x)
    #pass


def last_three_items(x):
    return x[-3:]
    #pass


def first_three_items(x):
    return x[:3]
    #pass


def sort_list(x):
    x.sort()
    return x
    #pass


def append_item(x, item):
    x.append(item)
    return x
    #pass


def remove_last_item(x):
    x.pop()
    return x
    #pass


def count_occurrences(x, item):
    return x.count(item)
    #pass


def is_item_present_in_list(x, item):
    return item in x
    #pass


def append_all_items_of_y_to_x(x, y):
    x.extend(y)
    return x
    """
    x and y are lists
    """
    #pass


def list_copy(x):
    y=x[:]
    return y
    """
    Create a shallow copy of x
    """

